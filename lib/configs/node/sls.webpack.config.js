/* eslint-disable import/no-commonjs */
const path = require("path");
const webpack = require("webpack");
const slsw = require("serverless-webpack");
const config = require("config");

module.exports = {
    entry: slsw.lib.entries,
    target: "node",
    externals: {
        knex: "commonjs knex" // https://github.com/tgriesser/knex/issues/1128#issuecomment-312735118
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loaders: ["babel-loader"],
                include: __dirname
            }
        ]
    },
    output: {
        libraryTarget: "commonjs",
        path: path.join(__dirname, ".webpack"),
        filename: "[name].js"
    },
    plugins: [
        new webpack.DefinePlugin({
            // double stringify because node-config expects this to be a string
            "process.env.NODE_CONFIG": JSON.stringify(JSON.stringify(config))
        })
    ]
};
