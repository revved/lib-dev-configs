/* eslint-disable import/no-commonjs */

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

let outputFilename = "[name].[contenthash].js"; // eslint-disable-line @revved/immutable/no-let
const idProd = process.env.NODE_ENV === "production";
const plugins = [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
        filename: path.resolve(__dirname, "dist/index.html"),
        template: path.resolve(__dirname, "public/index.html")
    })
];
if (!idProd) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
    outputFilename = "bundle.js";
}

module.exports = {
    mode: process.env.NODE_ENV || "development",
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                options: { presets: ["@babel/env"] }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
                include: /flexboxgrid/
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: "file-loader"
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx"]
    },
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        publicPath: "/",
        filename: outputFilename
    },
    optimization: {
        splitChunks: {
            chunks: "all"
        }
    },
    devServer: {
        contentBase: path.resolve(__dirname, "dist"),
        port: 3000,
        publicPath: "http://localhost:3000",
        hotOnly: true,
        historyApiFallback: true,
        compress: true
    },
    plugins
};
