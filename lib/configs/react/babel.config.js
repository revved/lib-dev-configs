// eslint-disable-next-line import/no-commonjs
module.exports = {
    presets: [
        [
            "@babel/preset-env",
            {
                targets: {
                    browsers: [">0.25%", "not ie 11", "not op_mini all"]
                }
            }
        ],
        "@babel/preset-react"
    ],
    plugins: [
        "babel-plugin-styled-components",
        "@babel/plugin-transform-runtime", // eslint-disable-line no-secrets/no-secrets
        "react-hot-loader/babel"
    ]
};
