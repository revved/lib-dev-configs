import fs from "fs-extra";
import merge from "lodash/merge";
import CONSTANTS from "./constants";

const PKG_PATH = `${process.cwd()}/package.json`;
const { PROJECT_TYPES, CONFIG_FILE_NAMES } = CONSTANTS;

const pkg = JSON.parse(fs.readFileSync(PKG_PATH, "utf8"));

class ScriptInstaller {
    addScripts(projectPreset) {
        let { scripts } = projectPreset;
        scripts = this.removeNonApplicableScripts(scripts);

        // Merge scripts
        const allScripts = merge(pkg.scripts, scripts);
        pkg.scripts = allScripts;

        // Merge config
        const allConfig = merge(pkg.config, this.getConfig());
        pkg.config = allConfig;

        // Write file
        fs.writeFileSync(PKG_PATH, JSON.stringify(pkg, null, 2));
    }

    getConfig() {
        return {
            commitizen: {
                path: "cz-conventional-changelog"
            }
        };
    }

    // - Remove test commands if no jest config file is found
    // - Remove build commands if no rollup config file is found
    removeNonApplicableScripts(scripts) {
        // TODO: add JEST_REACT when added
        if (
            this.shouldRemoveScriptBasedOnMissingFile(
                CONFIG_FILE_NAMES.JEST_NODE.OUT
            )
        ) {
            delete scripts.test;
            delete scripts["test:coverage:show"];
        }

        if (
            this.shouldRemoveScriptBasedOnMissingFile(
                CONFIG_FILE_NAMES.ROLLUP.OUT
            )
        ) {
            delete scripts.build;
            delete scripts.prepublish;
        }
        return scripts;
    }

    shouldRemoveScriptBasedOnMissingFile(fileName) {
        const path = `${process.cwd()}/${fileName}`;
        const shouldRemove = !fs.existsSync(path);

        return shouldRemove;
    }
}

export default new ScriptInstaller();
