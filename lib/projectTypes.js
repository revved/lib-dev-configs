import fs from "fs-extra";
import forEach from "lodash/forEach";
import presets from "./presets/presets";

export async function getProjectPresets() {
    const projectPresets = {};
    forEach(presets, preset => {
        projectPresets[preset.name] = preset;
    });
    return projectPresets;
}

export async function getProjectPreset(projectType) {
    await validateProjectType(projectType);
    const projectTypes = await getProjectPresets();
    const project = projectTypes[projectType];
    return project;
}

export async function getProjectTypes() {
    const projectTypes = await getProjectPresets();
    const names = Object.keys(projectTypes);
    return names;
}

async function validateProjectType(projectType) {
    const types = await getProjectTypes();
    const isValid = types.includes(projectType);
    if (!isValid) {
        throw new Error(
            `${projectType} is invalid. Must be one of ${types.join(", ")}.`
        );
    }
}
