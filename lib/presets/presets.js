import nodeLibPreset from "./nodeLib";
import nodeSlsApiPreset from "./nodeSlsApi";
import reactAppPreset from "./reactApp";

const presets = [nodeLibPreset, nodeSlsApiPreset, reactAppPreset];

export default presets;
