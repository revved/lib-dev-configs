import CONSTANTS from "../constants";

export default {
    name: "node_sls_api",
    summary: "Node.js Serverless API framework",
    path: "node",
    files: [
        CONSTANTS.CONFIG_FILE_NAMES.PRETTIER,
        CONSTANTS.CONFIG_FILE_NAMES.ESLINT_NODE,
        CONSTANTS.CONFIG_FILE_NAMES.ESLINT_IGNORE,
        CONSTANTS.CONFIG_FILE_NAMES.COMMITLINT,
        CONSTANTS.CONFIG_FILE_NAMES.GITIGNORE,
        CONSTANTS.CONFIG_FILE_NAMES.JEST_NODE,
        CONSTANTS.CONFIG_FILE_NAMES.GITLABCI_SLS,
        CONSTANTS.CONFIG_FILE_NAMES.BABEL_NODE,
        CONSTANTS.CONFIG_FILE_NAMES.HUSKY,
        CONSTANTS.CONFIG_FILE_NAMES.WEBPACK_SLS,
        CONSTANTS.CONFIG_FILE_NAMES.CODECLIMATE
    ],
    scripts: {
        test: "NODE_ENV=test jest",
        pretest:
            "npm run swagger:build && npm run link:aliases && npm run lint:quiet",
        postinstall: "npm run link:aliases",
        postci: "npm run link:aliases",
        "link:aliases": "link-module-alias",
        "swagger:build": "lib-swagger-tools bundle",
        "test:coverage:show": "open coverage/lcov-report/index.html",
        lint: "eslint ./lib",
        "lint:quiet": "eslint ./lib --quiet",
        "lint:all": "eslint .",
        "lint:all:quiet": "eslint . --quiet",
        "dev:setup:project":
            "npx @revved/lib-dev-configs@latest setup-project node_sls_api", // eslint-disable-line no-secrets/no-secrets
        "lint:commit": "commitlint -E HUSKY_GIT_PARAMS",
        commit: "git add . && git-cz",
        "_deploy:dev": "NODE_ENV=development sls deploy -s development",
        "deploy:dev":
            "echo 'There is no infra for this project. Skipping deploy for now.'",
        "postdeploy:dev": "npm run release",
        "_deploy:prod": "NODE_ENV=production sls deploy -s production",
        "deploy:prod":
            "echo 'There is no infra for this project. Skipping deploy for now.'",
        release: "standard-version",
        postrelease: "npm run release:tags",
        "release:tags": "git push origin HEAD && npm run release:gitlab",
        "release:gitlab": "conventional-gitlab-releaser -p angular",
        "tf:init": "cd terraform && terraform init",
        "tf:get": "cd terraform && terraform get -update",
        "tf:plan:dev":
            "cd terraform && terraform workspace select development && terraform plan -var-file=tfvars/development.tfvars",
        "tf:apply:dev":
            "cd terraform && terraform workspace select development && terraform apply -var-file=tfvars/development.tfvars",
        "tf:plan:prod":
            "cd terraform && terraform workspace select production && terraform plan -var-file=tfvars/production.tfvars",
        "tf:apply:prod":
            "cd terraform && terraform workspace select production && terraform apply -var-file=tfvars/production.tfvars"
    },
    dependencies: {
        "@babel/cli": "^7.4.4",
        "@babel/core": "^7.4.5",
        "@babel/preset-env": "^7.4.5",
        "@babel/node": "^7.4.5",
        "@commitlint/cli": "^8.0.0",
        "@commitlint/config-conventional": "^8.0.0",
        "@revved/eslint-config-node": "^1.0.19",
        "@revved/jest-config-node": "^1.1.2",
        jest: "^24.8.0",
        "jest-circus": "^24.8.0",
        "link-module-alias": "^1.2.0",
        prettier: "^1.17.1",
        serverless: "^1.47.0",
        "serverless-webpack": "^5.3.1",
        webpack: "^4.32.2",
        "webpack-node-externals": "^1.7.2",
        "@revved/prettier-preset": "^1.0.3",
        "js-pascalcase": "^1.2.1",
        "@revved/lib-rollup-config": "^1.1.5",
        "@revved/lib-swagger-tools": "^1.1.3",
        "babel-loader": "^8.0.6",
        husky: "^2.4.1",
        commitizen: "^3.1.1",
        "standard-version": "^6.0.1",
        "conventional-gitlab-releaser": "^4.0.1"
    }
};
