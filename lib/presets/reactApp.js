import CONSTANTS from "../constants";

export default {
    name: "react_app",
    summary: "Web app built using React",
    path: "node",
    files: [
        CONSTANTS.CONFIG_FILE_NAMES.PRETTIER,
        CONSTANTS.CONFIG_FILE_NAMES.ESLINT_REACT,
        CONSTANTS.CONFIG_FILE_NAMES.ESLINT_IGNORE,
        CONSTANTS.CONFIG_FILE_NAMES.COMMITLINT,
        CONSTANTS.CONFIG_FILE_NAMES.GITIGNORE,
        CONSTANTS.CONFIG_FILE_NAMES.JEST_REACT,
        CONSTANTS.CONFIG_FILE_NAMES.GITLABCI_REACT,
        CONSTANTS.CONFIG_FILE_NAMES.BABEL_REACT,
        CONSTANTS.CONFIG_FILE_NAMES.HUSKY,
        CONSTANTS.CONFIG_FILE_NAMES.WEBPACK_REACT,
        CONSTANTS.CONFIG_FILE_NAMES.CODECLIMATE
    ],
    scripts: {
        pretest: "npm run link:aliases && npm run lint:quiet",
        postinstall: "npm run link:aliases",
        postci: "npm run link:aliases",
        "prelink:aliases": "npm run build:style",
        "link:aliases": "link-module-alias",
        "build:style":
            "style-dictionary build --config ./src/style-dictionary/config.js",
        prebuild: "npm run link:aliases",
        build: "webpack",
        "build:profile":
            "NODE_ENV=production webpack --profile --json > dist/stats.json && webpack-bundle-analyzer dist/stats.json",
        prestart: "npm run link:aliases",
        start: "NODE_ENV=development webpack-dev-server --mode development",
        test: "NODE_ENV=test jest",
        "test:coverage:show": "open coverage/lcov-report/index.html",
        lint: "eslint ./src",
        "lint:quiet": "eslint ./src --quiet",
        "lint:all": "eslint .",
        "lint:all:quiet": "eslint . --quiet",
        "lint:commit": "commitlint -E HUSKY_GIT_PARAMS",
        commit: "git add . && git-cz",
        release: "standard-version",
        "release:gitlab": "conventional-gitlab-releaser -p angular",
        postrelease: "npm run release:tags",
        "release:tags": "git push origin HEAD && npm run release:gitlab",
        "predeploy:dev": "NODE_ENV=development npm run build",
        "_deploy:dev": "NODE_ENV=development babel-node ./bin/deploy.js",
        "deploy:dev": "echo 'There is no infra for this project. Skipping'",
        "postdeploy:dev": "npm run release",
        "predeploy:prod": "NODE_ENV=production npm run build",
        "_deploy:prod": "NODE_ENV=production babel-node ./bin/deploy.js",
        "deploy:prod": "echo 'There is no infra for this project. Skipping'",
        "dev:setup:project":
            "npx @revved/lib-dev-configs@latest setup-project react_app", // eslint-disable-line no-secrets/no-secrets
        "tf:init": "cd terraform && terraform init",
        "tf:get": "cd terraform && terraform get -update",
        "tf:plan:dev":
            "cd terraform && terraform workspace select development && terraform plan -var-file=tfvars/development.tfvars",
        "tf:apply:dev":
            "cd terraform && terraform workspace select development && terraform apply -var-file=tfvars/development.tfvars",
        "tf:plan:prod":
            "cd terraform && terraform workspace select production && terraform plan -var-file=tfvars/production.tfvars",
        "tf:apply:prod":
            "cd terraform && terraform workspace select production && terraform apply -var-file=tfvars/production.tfvars"
    },
    dependencies: {
        "@babel/cli": "^7.4.4",
        "@babel/core": "^7.4.5",
        "@babel/preset-env": "^7.4.5",
        "@babel/node": "^7.4.5",
        "@commitlint/cli": "^8.0.0",
        "@commitlint/config-conventional": "^8.0.0",
        "@revved/eslint-config-react": "^1.0.0",
        "@revved/jest-config-react": "^1.1.9",
        jest: "^24.8.0",
        "jest-circus": "^24.8.0",
        "link-module-alias": "^1.2.0",
        prettier: "^1.17.1",
        serverless: "^1.47.0",
        "serverless-webpack": "^5.3.1",
        webpack: "^4.32.2",
        "webpack-node-externals": "^1.7.2",
        "@revved/prettier-preset": "^1.0.3",
        "js-pascalcase": "^1.2.1",
        "babel-loader": "^8.0.6",
        husky: "^2.4.1",
        commitizen: "^3.1.1",
        "standard-version": "^6.0.1",
        "conventional-gitlab-releaser": "^4.0.1",
        "html-webpack-plugin": "^3.2.0",
        "clean-webpack-plugin": "^3.0.0"
    }
};
