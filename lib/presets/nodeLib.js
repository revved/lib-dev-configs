import CONSTANTS from "../constants";

export default {
    name: "node_lib",
    summary: "Node.js Library",
    path: "node",
    files: [
        CONSTANTS.CONFIG_FILE_NAMES.PRETTIER,
        CONSTANTS.CONFIG_FILE_NAMES.ESLINT_NODE,
        CONSTANTS.CONFIG_FILE_NAMES.ESLINT_IGNORE,
        CONSTANTS.CONFIG_FILE_NAMES.COMMITLINT,
        CONSTANTS.CONFIG_FILE_NAMES.GITIGNORE,
        CONSTANTS.CONFIG_FILE_NAMES.JEST_NODE,
        CONSTANTS.CONFIG_FILE_NAMES.GITLABCI_LIB,
        CONSTANTS.CONFIG_FILE_NAMES.BABEL_NODE,
        CONSTANTS.CONFIG_FILE_NAMES.HUSKY,
        CONSTANTS.CONFIG_FILE_NAMES.ROLLUP,
        CONSTANTS.CONFIG_FILE_NAMES.CODECLIMATE
    ],
    scripts: {
        pretest: "npm run lint:quiet",
        test: "jest",
        "test:coverage:show": "open coverage/lcov-report/index.html",
        lint: "eslint ./lib",
        "lint:quiet": "eslint ./lib --quiet",
        "lint:all": "eslint .",
        "lint:all:quiet": "eslint . --quiet",
        "lint:commit": "commitlint -E HUSKY_GIT_PARAMS",
        commit: "git add . && git-cz",
        release: "standard-version",
        "release:gitlab": "conventional-gitlab-releaser -p angular",
        postrelease: "npm run publish:custom && npm run release:tags",
        "release:tags": "git push origin HEAD && npm run release:gitlab",
        "dev:setup:project":
            "npx @revved/lib-dev-configs@latest setup-project node_lib", // eslint-disable-line no-secrets/no-secrets
        build: "rollup --config",
        "publish:custom": "npm publish",
        prerelease: "npm run build"
    },
    dependencies: {
        "@babel/cli": "^7.4.4",
        "@babel/core": "^7.4.5",
        "@babel/preset-env": "^7.4.5",
        "@babel/node": "^7.4.5",
        "@commitlint/cli": "^8.0.0",
        "@commitlint/config-conventional": "^8.0.0",
        "@revved/eslint-config-node": "^1.0.19",
        "@revved/jest-config-node": "^1.1.2",
        jest: "^24.8.0",
        "jest-circus": "^24.8.0",
        prettier: "^1.17.1",
        "@revved/prettier-preset": "^1.0.3",
        "js-pascalcase": "^1.2.1",
        "@revved/lib-rollup-config": "^1.1.5",
        rollup: "^1.14.2",
        "babel-loader": "^8.0.6",
        husky: "^2.4.1",
        commitizen: "^3.1.1",
        "standard-version": "^6.0.1",
        "conventional-gitlab-releaser": "^4.0.1"
    }
};
