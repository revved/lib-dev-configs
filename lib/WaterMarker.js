import fs from "fs-extra";
import prependFile from "prepend-file";
import includes from "lodash/includes";
import CONSTANTS from "./constants";

const { CONFIG_FILE_NAMES } = CONSTANTS;

class WaterMarker {
    watermarkFile(outFilePath) {
        const watermark = this.getWatermark(outFilePath);
        prependFile.sync(outFilePath, watermark);
    }

    getWatermark(outFilePath) {
        const commonWatermark = this.getWatermarkWithoutCommentChars(
            outFilePath
        );
        const commentChars = this.getWatermarkCommentChars(outFilePath);
        const watermark = `${commentChars} ${commonWatermark}\n\n`;
        return watermark;
    }

    getWatermarkCommentChars(outFilePath) {
        const pathParts = outFilePath.split("/");
        const outFileName = pathParts.pop();
        const fileExt = outFileName.split(".").pop();

        let commentChars = "//";
        if (
            outFileName === CONFIG_FILE_NAMES.GITIGNORE.OUT ||
            outFileName === CONFIG_FILE_NAMES.ESLINT_IGNORE.OUT ||
            fileExt === "yml"
        ) {
            commentChars = "#";
        }
        return commentChars;
    }

    getWatermarkWithoutCommentChars(outFilePath, shouldIgnoreVersion) {
        const managedDisclaimer = "File managed by";

        const { name, version } = this.getThisPackageNameAndVersion();
        let waterMark = `${managedDisclaimer} ${name}`;
        if (!shouldIgnoreVersion) {
            waterMark += `@${version}`;
        }

        return waterMark;
    }

    getThisPackageNameAndVersion() {
        const thisPackage = JSON.parse(
            fs.readFileSync(`${__dirname}/../package.json`, "utf8")
        );
        const thisPackageName = thisPackage.name;
        const thisPackageVersion = thisPackage.version;

        return {
            name: thisPackageName,
            version: thisPackageVersion
        };
    }

    hasWaterMarkOrDoesntExist(outFilePath) {
        let curentFile;
        try {
            curentFile = fs.readFileSync(
                `${process.cwd()}/${outFilePath}`,
                "utf8"
            );
        } catch (e) {
            // File doesn't exist, will need to install
            return true;
        }

        const waterMarkWithoutCommentCharsAndVersion = this.getWatermarkWithoutCommentChars(
            outFilePath,
            true
        );

        const hasIt = includes(
            curentFile,
            waterMarkWithoutCommentCharsAndVersion
        );
        return hasIt;
    }
}

export default new WaterMarker();
