import shell from "shelljs";

class ShellHelper {
    async exec(command, opts) {
        // Let's wrap `exec` in a promise. For this we need a new promise.
        // So we disable eslint so it doesn't complain for using new Promise
        // eslint-disable-next-line
        return new Promise((resolve, reject) =>
            shell.exec(
                command,
                opts || { silent: true, async: true },
                (code, stdout, stderr) => {
                    if (code !== 0) {
                        const newErr = new Error(stderr);
                        return reject(newErr);
                    }
                    return resolve(stdout);
                }
            )
        );
    }

    cd(path) {
        return shell.cd(path);
    }
}
export default new ShellHelper();
