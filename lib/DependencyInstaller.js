import forEach from "lodash/forEach";

import shell from "./ShellHelper";

class DependencyInstaller {
    // get dependencies
    getInstallCommand(projectPreset) {
        const { dependencies } = projectPreset;
        let command = "npm install --save-dev";

        forEach(dependencies, (value, key) => {
            let version = value;
            version.replace('"', "");
            version = `@${version}`;
            command += ` ${key}${version}`;
        });

        return command;
    }

    async addDevDependencies(projectPreset) {
        const cmd = this.getInstallCommand(projectPreset);
        await shell.exec(cmd);
        return true;
    }
}

export default new DependencyInstaller();
