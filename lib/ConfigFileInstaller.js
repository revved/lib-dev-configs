import fs from "fs-extra";
import forEach from "lodash/forEach";
import WaterMarker from "./WaterMarker";

class ConfigFileInstaller {
    addConfigFiles(projectPreset) {
        const filePresetsForProjectType = projectPreset.files;
        const didInstallMap = {};

        forEach(filePresetsForProjectType, configForFile => {
            const didInstallFile = this.installFile(
                configForFile.PATH,
                configForFile
            );
            didInstallMap[configForFile.OUT] = didInstallFile;
        });

        return didInstallMap;
    }

    installFile(configPath, configForFile) {
        const inFile = `${__dirname}/../lib/configs/${configPath}/${
            configForFile.IN
        }`;
        const outFile = `${process.cwd()}/${configForFile.OUT}`;

        const shouldInstall = this.shouldInstallFile(configForFile.OUT);

        const thisPkgName = WaterMarker.getThisPackageNameAndVersion().name;
        if (!shouldInstall) {
            console.log(
                `File ${
                    configForFile.OUT
                } was not installed as it's not managed by ${thisPkgName}`
            );
            return false;
        }

        fs.copySync(inFile, outFile);
        WaterMarker.watermarkFile(outFile);
        return true;
    }

    shouldInstallFile(outFilePath) {
        return WaterMarker.hasWaterMarkOrDoesntExist(outFilePath);
    }
}

export default new ConfigFileInstaller();
