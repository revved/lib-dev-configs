const PROJECT_TYPES = {
    NODE_LIB: "node_lib",
    NODE_SLS_API: "node_sls_api",
    REACT_APP: "react_app"
};

const CONFIG_FILE_NAMES = {
    JEST_NODE: { IN: "jest.config.js", OUT: "jest.config.js", PATH: "node" },
    JEST_REACT: { IN: "jest.config.js", OUT: "jest.config.js", PATH: "react" },
    ROLLUP: { IN: "rollup.config.js", OUT: "rollup.config.js", PATH: "node" },
    PRETTIER: { IN: ".prettierrc.js", OUT: ".prettierrc.js", PATH: "common" },
    BABEL_NODE: { IN: "babel.config.js", OUT: "babel.config.js", PATH: "node" },
    BABEL_REACT: {
        IN: "babel.config.js",
        OUT: "babel.config.js",
        PATH: "react"
    },
    WEBPACK_SLS: {
        IN: "sls.webpack.config.js",
        OUT: "webpack.config.js",
        PATH: "node"
    },
    WEBPACK_REACT: {
        IN: "webpack.config.js",
        OUT: "webpack.config.js",
        PATH: "react"
    },
    GITIGNORE: { IN: ".gitignore_template", OUT: ".gitignore", PATH: "common" },
    ESLINT_NODE: {
        IN: ".eslintrc_template.js",
        OUT: ".eslintrc.js",
        PATH: "node"
    },
    ESLINT_REACT: {
        IN: ".eslintrc_template.js",
        OUT: ".eslintrc.js",
        PATH: "react"
    },
    ESLINT_IGNORE: {
        IN: ".eslintignore_template",
        OUT: ".eslintignore",
        PATH: "common"
    },
    HUSKY: { IN: ".huskyrc.js", OUT: ".huskyrc.js", PATH: "common" },
    COMMITLINT: {
        IN: "commitlint.config.js",
        OUT: "commitlint.config.js",
        PATH: "common"
    },
    GITLABCI_LIB: {
        IN: "lib.gitlab-ci.yml",
        OUT: ".gitlab-ci.yml",
        PATH: "node"
    },
    GITLABCI_REACT: {
        IN: "gitlab-ci.yml",
        OUT: ".gitlab-ci.yml",
        PATH: "react"
    },
    GITLABCI_SLS: {
        IN: "sls.gitlab-ci.yml",
        OUT: ".gitlab-ci.yml",
        PATH: "node"
    },
    CODECLIMATE: {
        IN: ".codeclimate.yml",
        OUT: ".codeclimate.yml",
        PATH: "common"
    }
};

export default {
    PROJECT_TYPES,
    CONFIG_FILE_NAMES
};
