// File managed by @revved/lib-dev-configs@2.9.3

// eslint-disable-next-line import/no-commonjs
module.exports = {
    hooks: {
        "pre-commit": "npm run lint:all:quiet",
        "pre-push": "npm test",
        "commit-msg": "npm run lint:commit"
    }
};
