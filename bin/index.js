#!/usr/bin/env node
/* eslint-disable import/no-commonjs */
import program from "commander";
import chalk from "chalk";

import ScriptInstaller from "../lib/ScriptInstaller";
import ConfigFileInstaller from "../lib/ConfigFileInstaller";
import DependencyInstaller from "../lib/DependencyInstaller";
import { getProjectPreset } from "../lib/projectTypes";

program
    .command("setup-project <projectType>")
    .action(async (projectType, cmd) => {
        try {
            const projectPreset = await getProjectPreset(projectType);
            await ConfigFileInstaller.addConfigFiles(projectPreset);
            await ScriptInstaller.addScripts(projectPreset);
            await DependencyInstaller.addDevDependencies(projectPreset);
        } catch (e) {
            handleError(e);
        }
    });

program
    .command("add-scripts <projectType>")
    .action(async (projectType, cmd) => {
        try {
            const projectPreset = await getProjectPreset(projectType);
            await ScriptInstaller.addScripts(projectPreset);
        } catch (e) {
            handleError(e);
        }
    });

program
    .command("add-config-files <projectType>")
    .action(async (projectType, cmd) => {
        try {
            const projectPreset = await getProjectPreset(projectType);
            await ConfigFileInstaller.addConfigFiles(projectPreset);
        } catch (e) {
            handleError(e);
        }
    });

program
    .command("add-dev-deps <projectType>")
    .action(async (projectType, cmd) => {
        try {
            const projectPreset = await getProjectPreset(projectType);
            await DependencyInstaller.addDevDependencies(projectPreset);
        } catch (e) {
            handleError(e);
        }
    });

// Handle unknown commands
program.on("command:*", () => {
    let errMsg = `Invalid command: ${program.args.join(" ")}\n`;
    errMsg += "See --help for a list of available commands.";
    const error = new Error(errMsg);
    handleError(error);
});

program.parse(process.argv);
if (!program.args.length) {
    program.help();
}

process.on("uncaughtException", err => {
    handleError(err);
});

function handleError(e) {
    console.log(chalk.red(e.message));
    process.exit(e);
}
