# lib-dev-configs

[![coverage](https://gitlab.com/revved/lib-dev-configs/badges/master/coverage.svg?style=flat)](https://revved.gitlab.io/lib-dev-configs/) ![build](https://gitlab.com/revved/lib-dev-configs/badges/master/pipeline.svg)

Easily install `Revved` dev dependencies, config files, and npm scripts.

### Setup Project.

`npx @revved/lib-dev-configs@latest setup-project node_lib`

### Add dev dependencies

`npx @revved/lib-dev-configs@latest add-dev-deps node_lib`

### Add NPM scripts

`npx @revved/lib-dev-configs@latest add-scripts node_lib`

### Add Config Files

`npx @revved/lib-dev-configs@latest add-config-files node_lib`

### Test cli locally

`babel-node ./bin/index.js setup-project node_lib`
