module.exports = {
    extends: ["@revved/node"],
    rules: {
        "import/no-unresolved": "off",
        "lodash/prefer-lodash-method": "off"
    }
};
