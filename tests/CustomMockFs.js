// https://github.com/tschaub/mock-fs/issues/234#issuecomment-446980942
import mockFs from "mock-fs";

let logsTemp = [];
let logMock;

function CustomMockFs(config) {
    logMock = jest.spyOn(console, "log").mockImplementation((...args) => {
        logsTemp.push(args);
    });
    mockFs(config);
}

CustomMockFs.restore = () => {
    // if (logMock) {
    logMock.mockRestore();
    // }

    mockFs.restore();
    logsTemp.map(el => console.log(...el));
    logsTemp = [];
};

export default CustomMockFs;
