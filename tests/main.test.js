import fs from "fs-extra";
import includes from "lodash/includes";
import forEach from "lodash/forEach";
import split from "lodash/split";
import join from "lodash/join";
import Promise from "bluebird";

import mockFs from "./CustomMockFs";
import ScriptInstaller from "../lib/ScriptInstaller";
import ConfigFileInstaller from "../lib/ConfigFileInstaller";
import WaterMarker from "../lib/WaterMarker";
import CONSTANTS from "../lib/constants";
import {
    getProjectPresets,
    getProjectPreset,
    getProjectTypes
} from "../lib/projectTypes";

const CONFIGS_FOLDER = `${process.cwd()}/lib/configs`;
const PRESETS_FOLDER = `${process.cwd()}/lib/presets`;

beforeAll(async () => {
    const mockedFiles = getOriginalFiles();
    // You but get the project presets before doing mocked files so that babel can run the requires on projectTypes.js
    await getProjectPresets();

    mockFs(mockedFiles);
});

afterAll(async () => {
    mockFs.restore();
});

// loop through each project to make sure it has
// a name, files, a path, dependencies, and scripts
test.each(Object.values(CONSTANTS.PROJECT_TYPES))(
    "make sure presets are defined for %s",
    async projectType => {
        await getProjectTypeAndAssert(projectType);
    }
);

// loop through each project to install
// scripts and files
test.each(Object.values(CONSTANTS.PROJECT_TYPES))(
    "setup project for %s",
    async projectType => {
        await setupProjectAndAssert(projectType);
    }
);

// try to install with an incorrect project type
test("fail to install config and add scripts for invalid project type", async () => {
    const projectType = "invalid_type";

    let expectedErrorA;
    try {
        await getProjectPreset(projectType);
    } catch (e) {
        expectedErrorA = e;
    }
    const oneOf = await getProjectTypes();
    const expectedMessage = `invalid_type is invalid. Must be one of ${oneOf.join(
        ", "
    )}.`;
    expect(expectedErrorA).toBeDefined();
    expect(expectedErrorA.message).toBe(expectedMessage);
});

test("try to install over file that's managed by this project", async () => {
    // Install preset
    const projectType = CONSTANTS.PROJECT_TYPES.NODE_LIB;
    const projectPreset = await getProjectPreset(projectType);
    const didInstallMap = await ConfigFileInstaller.addConfigFiles(
        projectPreset
    );

    assertDidInstallMapAllBool(didInstallMap, true);

    // Now mock having a new version of this project
    const versionAndName = WaterMarker.getThisPackageNameAndVersion();
    versionAndName.version = "10.0.0";
    WaterMarker.getThisPackageNameAndVersion = () => {
        return versionAndName;
    };

    // Now install preset again
    const didInstallMapB = await ConfigFileInstaller.addConfigFiles(
        projectPreset
    );
    assertDidInstallMapAllBool(didInstallMapB, true);

    // Read one of the files and assert new water mark
    const filePath = `${process.cwd()}/${
        CONSTANTS.CONFIG_FILE_NAMES.JEST_NODE.OUT
    }`;
    const fileContent = await readFileSync(filePath);
    expect(doesHaveWaterMark(fileContent, versionAndName)).toBe(true);

    // Remove water mark for jest file
    const fileLines = split(fileContent, "\n");
    // Remove first element
    fileLines.shift();
    // Join again
    const fileContentsWithoutWaterMark = join(fileLines, "\n");
    // Write file
    fs.writeFileSync(filePath, fileContentsWithoutWaterMark);

    // Install yet again, but assert that all were installed except for jest
    const didInstallMapC = await ConfigFileInstaller.addConfigFiles(
        projectPreset
    );
    expect(didInstallMapC[CONSTANTS.CONFIG_FILE_NAMES.JEST_NODE.OUT]).toBe(
        false
    );
    delete didInstallMapC[CONSTANTS.CONFIG_FILE_NAMES.JEST_NODE.OUT];
    assertDidInstallMapAllBool(didInstallMapB, true);

    // Lastly, read file again and don't expect watermark
    const fileContentB = readFileSync(filePath);
    expect(doesHaveWaterMark(fileContentB, versionAndName)).toBe(false);
});

test("watermark comment char", async () => {
    // Make sure we get the right comment char for reach file type
    const configFilesAndExpectedChars = [
        { file: CONSTANTS.CONFIG_FILE_NAMES.JEST_NODE.OUT, expectedChar: "//" },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.JEST_REACT.OUT,
            expectedChar: "//"
        },
        { file: CONSTANTS.CONFIG_FILE_NAMES.ROLLUP.OUT, expectedChar: "//" },
        { file: CONSTANTS.CONFIG_FILE_NAMES.PRETTIER.OUT, expectedChar: "//" },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.BABEL_NODE.OUT,
            expectedChar: "//"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.BABEL_REACT.OUT,
            expectedChar: "//"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.WEBPACK_SLS.OUT,
            expectedChar: "//"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.WEBPACK_REACT.OUT,
            expectedChar: "//"
        },
        { file: CONSTANTS.CONFIG_FILE_NAMES.GITIGNORE.OUT, expectedChar: "#" },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.ESLINT_NODE.OUT,
            expectedChar: "//"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.ESLINT_REACT.OUT,
            expectedChar: "//"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.ESLINT_IGNORE.OUT,
            expectedChar: "#"
        },
        { file: CONSTANTS.CONFIG_FILE_NAMES.HUSKY.OUT, expectedChar: "//" },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.COMMITLINT.OUT,
            expectedChar: "//"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.GITLABCI_LIB.OUT,
            expectedChar: "#"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.GITLABCI_SLS.OUT,
            expectedChar: "#"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.GITLABCI_REACT.OUT,
            expectedChar: "#"
        },
        {
            file: CONSTANTS.CONFIG_FILE_NAMES.CODECLIMATE.OUT,
            expectedChar: "#"
        }
    ];

    configFilesAndExpectedChars.forEach(pair => {
        const waterMarkChar = WaterMarker.getWatermarkCommentChars(pair.file);
        expect(pair.expectedChar).toBe(waterMarkChar);
    });

    // Also make sure we asserted all file types
    expect(configFilesAndExpectedChars.length).toBe(
        Object.keys(CONSTANTS.CONFIG_FILE_NAMES).length
    );
});

// IMPORTANT: leave for last
test("no removal of applicable scripts", async () => {
    // Mock fs again, but with no files
    mockFs({});

    // Try to remove non applicable scripts.
    // Because there's a rollup and a jest file
    // we don't need to remove the scripts in question
    ScriptInstaller.removeNonApplicableScripts({});

    // Restore again
    mockFs.restore();
});

function doesHaveWaterMark(fileContent, versionAndName) {
    return includes(
        fileContent,
        `File managed by ${versionAndName.name}@${versionAndName.version}`
    );
}

function getOriginalFiles() {
    const fileMocks = {};

    fs.readdirSync(CONFIGS_FOLDER).forEach(dirName => {
        const dirPath = `${CONFIGS_FOLDER}/${dirName}`;
        fs.readdirSync(dirPath).forEach(fileName => {
            const filePath = `${dirPath}/${fileName}`;
            fileMocks[filePath] = readFileSync(filePath);
        });
    });
    fs.readdirSync(PRESETS_FOLDER).forEach(fileName => {
        const filePath = `${PRESETS_FOLDER}/${fileName}`;
        fileMocks[filePath] = readFileSync(filePath);
    });

    const pkgPath = `${process.cwd()}/package.json`;
    fileMocks[pkgPath] = readFileSync(pkgPath);
    return fileMocks;
}

function readFileSync(path) {
    return fs.readFileSync(path, "utf8");
}

function assertDidInstallMapAllBool(didInstallMap, boolVal) {
    forEach(didInstallMap, didInstallFile => {
        expect(didInstallFile).toBe(boolVal);
    });
}
async function setupProjectAndAssert(projectType) {
    const projectPreset = await getProjectPreset(projectType);
    await ConfigFileInstaller.addConfigFiles(projectPreset);
    await ScriptInstaller.addScripts(projectPreset);
}

async function getProjectTypeAndAssert(projectType) {
    const projectPreset = await getProjectPreset(projectType);
    expect(projectPreset.files).toBeDefined();
    expect(projectPreset.scripts).toBeDefined();
    expect(projectPreset.path).toBeDefined();
    expect(projectPreset.dependencies).toBeDefined();
    expect(projectPreset.name).toBe(projectType);
}
