import tmp from "tmp-promise";
import startsWith from "lodash/startsWith";
import cloneDeep from "lodash/cloneDeep";

import chalk from "chalk";

import shell from "../lib/ShellHelper";
import { getProjectPreset } from "../lib/projectTypes";
import DependencyInstaller from "../lib/DependencyInstaller";
import CONSTANTS from "../lib/constants";

jest.setTimeout(200000);

let dir;
const execBak = cloneDeep(shell);

// Before each test create a tmp directory to be able
// to install the deps into
beforeEach(async () => {
    dir = await tmp.dir({ unsafeCleanup: true });
});

// After each test cleanup the tmp directory
afterEach(async () => {
    await dir.cleanup();
});

test.each(Object.values(CONSTANTS.PROJECT_TYPES))(
    "mock installing dependencies for %s",
    async projectType => {
        console.log("Installing dependencies for", projectType);

        // Get presets and install the dependencies
        const projectPreset = await getProjectPreset(projectType);
        const installCommand = await DependencyInstaller.getInstallCommand(
            projectPreset
        );

        expect(startsWith(installCommand, "npm install --save-dev")).toBe(true);

        // Mock exec
        shell.exec = () => {
            console.log(`Mock exec running ${installCommand}`);
        };
        await DependencyInstaller.addDevDependencies(projectPreset);

        // Restore exec
        shell.exec = execBak.exec;
    }
);

test("install non-existant dependency", async () => {
    // Change cd to the tmp directory and create a package.json file
    shell.cd(dir.path);
    await shell.exec("npm init -y");

    // Install fake presets and expect an error
    const fakeProjectPreset = {
        name: "node_lib",
        dependencies: {
            fakeyfakeybakeybakey: "^7.4.4",
            fooltoolfooltool: "^7.4.5"
        }
    };
    let expectedError;
    try {
        await DependencyInstaller.addDevDependencies(fakeProjectPreset);
    } catch (e) {
        expectedError = e;
    }
    expect(expectedError).toBeDefined();
});
