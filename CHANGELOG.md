# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.9.4](https://gitlab.com/revved/lib-dev-configs/compare/v2.9.3...v2.9.4) (2019-09-24)



### [2.9.3](https://gitlab.com/revved/lib-dev-configs/compare/v2.9.2...v2.9.3) (2019-09-24)


### Bug Fixes

* ignore .terraform in gitignore ([b9d307e](https://gitlab.com/revved/lib-dev-configs/commit/b9d307e))



### [2.9.2](https://gitlab.com/revved/lib-dev-configs/compare/v2.9.1...v2.9.2) (2019-09-12)



### [2.9.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.9.0...v2.9.1) (2019-09-10)


### Bug Fixes

* and again ([720035c](https://gitlab.com/revved/lib-dev-configs/commit/720035c))



## [2.9.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.8.3...v2.9.0) (2019-09-10)


### Features

* upgraded @revved/jest-config-react yet again ([67bc556](https://gitlab.com/revved/lib-dev-configs/commit/67bc556))



### [2.8.3](https://gitlab.com/revved/lib-dev-configs/compare/v2.8.2...v2.8.3) (2019-09-09)



### [2.8.2](https://gitlab.com/revved/lib-dev-configs/compare/v2.8.1...v2.8.2) (2019-09-09)



### [2.8.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.8.0...v2.8.1) (2019-09-07)


### Bug Fixes

* fixed versions of two dev deps for react preset ([78ef29f](https://gitlab.com/revved/lib-dev-configs/commit/78ef29f))



## [2.8.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.20...v2.8.0) (2019-09-06)


### Bug Fixes

* moved path to constants file for each config file ([1a34dfa](https://gitlab.com/revved/lib-dev-configs/commit/1a34dfa))


### Features

* react configs ([63b5c7d](https://gitlab.com/revved/lib-dev-configs/commit/63b5c7d))
* started working towards react configs ([c0904c3](https://gitlab.com/revved/lib-dev-configs/commit/c0904c3))



### [2.5.20](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.19...v2.5.20) (2019-08-09)



### [2.5.19](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.18...v2.5.19) (2019-07-31)



### [2.5.18](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.17...v2.5.18) (2019-07-31)


### Bug Fixes

* use release instead of deploy in gitlab file for sls api ([0e34510](https://gitlab.com/revved/lib-dev-configs/commit/0e34510))



### [2.5.17](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.15...v2.5.17) (2019-07-31)


### Bug Fixes

* deleted versionrc file ([1c353ca](https://gitlab.com/revved/lib-dev-configs/commit/1c353ca))
* install missing dep ([a776521](https://gitlab.com/revved/lib-dev-configs/commit/a776521))



### [2.5.16](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.15...v2.5.16) (2019-07-31)


### Bug Fixes

* deleted versionrc file ([1c353ca](https://gitlab.com/revved/lib-dev-configs/commit/1c353ca))



### [2.5.15](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.14...v2.5.15) (2019-07-31)


### Docs

* dummy docs change ([ea84793](https://gitlab.com/revved/lib-dev-configs/commit/ea84793))



### [2.5.14](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.13...v2.5.14) (2019-07-31)


### Chores

* trying to make all sections show up in changelog ([5d599e6](https://gitlab.com/revved/lib-dev-configs/commit/5d599e6))



### [2.5.13](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.12...v2.5.13) (2019-07-31)


### Bug Fixes

* dummy fix change ([6b254c5](https://gitlab.com/revved/lib-dev-configs/commit/6b254c5))



### [2.5.12](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.11...v2.5.12) (2019-07-31)



### [2.5.11](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.10...v2.5.11) (2019-07-31)



### [2.5.10](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.9...v2.5.10) (2019-07-31)



### [2.5.9](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.8...v2.5.9) (2019-07-31)



### [2.5.8](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.7...v2.5.8) (2019-07-30)



### [2.4.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.7...v2.4.1) (2019-07-30)



### [2.5.7](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.6...v2.5.7) (2019-07-30)



### [2.5.6](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.5...v2.5.6) (2019-07-30)



### [2.5.5](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.4...v2.5.5) (2019-07-30)



### [2.5.4](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.3...v2.5.4) (2019-07-30)



### [2.5.3](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.2...v2.5.3) (2019-07-30)



### [2.5.2](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.1...v2.5.2) (2019-07-30)



### [2.5.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.5.0...v2.5.1) (2019-07-30)



## [2.5.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.4.3...v2.5.0) (2019-07-30)


### Bug Fixes

* remove -r flag ([cbb03db](https://gitlab.com/revved/lib-dev-configs/commit/cbb03db))


### Features

* gitlab releases ([c5a1fcc](https://gitlab.com/revved/lib-dev-configs/commit/c5a1fcc))



### [2.4.3](https://gitlab.com/revved/lib-dev-configs/compare/v2.4.2...v2.4.3) (2019-07-30)


### Tests

* speed up tests ([9b71f61](https://gitlab.com/revved/lib-dev-configs/commit/9b71f61))



### [2.4.2](https://gitlab.com/revved/lib-dev-configs/compare/v2.4.1...v2.4.2) (2019-07-30)


### Bug Fixes

* clean up dev deps ([7d25c2b](https://gitlab.com/revved/lib-dev-configs/commit/7d25c2b))



### [2.4.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.4.0...v2.4.1) (2019-07-30)


### Bug Fixes

* remove build and publish in favor of deploy script ([d47b362](https://gitlab.com/revved/lib-dev-configs/commit/d47b362))



## [2.4.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.3.0...v2.4.0) (2019-07-30)


### Bug Fixes

* fixed timeout used in dep suite ([1823e53](https://gitlab.com/revved/lib-dev-configs/commit/1823e53))
* remove water mark for raw file ([eb92db0](https://gitlab.com/revved/lib-dev-configs/commit/eb92db0))


### Features

* use new jest ([54a5f9d](https://gitlab.com/revved/lib-dev-configs/commit/54a5f9d))



## [2.3.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.2.0...v2.3.0) (2019-07-30)


### Bug Fixes

* use the right comment char for codeclimate file ([620339d](https://gitlab.com/revved/lib-dev-configs/commit/620339d))


### Features

* better gitlab files ([b4c956a](https://gitlab.com/revved/lib-dev-configs/commit/b4c956a))



## [2.2.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.1.1...v2.2.0) (2019-07-29)


### Features

* codeclimate configs ([20a2af9](https://gitlab.com/revved/lib-dev-configs/commit/20a2af9))



### [2.1.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.1.0...v2.1.1) (2019-07-24)


### Bug Fixes

* use managed config files when possible ([960542d](https://gitlab.com/revved/lib-dev-configs/commit/960542d))



## [2.1.0](https://gitlab.com/revved/lib-dev-configs/compare/v2.0.2...v2.1.0) (2019-07-24)


### Bug Fixes

* fixed own gitlab-ci file ([e843490](https://gitlab.com/revved/lib-dev-configs/commit/e843490))
* use correct gitlab file for sls api ([0ad1feb](https://gitlab.com/revved/lib-dev-configs/commit/0ad1feb))


### Features

* added sls.gitlab-ci.yaml ([bb89570](https://gitlab.com/revved/lib-dev-configs/commit/bb89570))
* added webpack to gitignore ([59b892b](https://gitlab.com/revved/lib-dev-configs/commit/59b892b))



### [2.0.2](https://gitlab.com/revved/lib-dev-configs/compare/v2.0.1...v2.0.2) (2019-07-23)


### Bug Fixes

* **presetshelper:** added a rollup helper file ([4c014b1](https://gitlab.com/revved/lib-dev-configs/commit/4c014b1))
* **projecttypes:** removed dynamic require ([32ac002](https://gitlab.com/revved/lib-dev-configs/commit/32ac002))



### [2.0.1](https://gitlab.com/revved/lib-dev-configs/compare/v2.0.0...v2.0.1) (2019-07-23)


### Bug Fixes

* **shelljs:** added to dependencies ([8591a63](https://gitlab.com/revved/lib-dev-configs/commit/8591a63))



## [2.0.0](https://gitlab.com/revved/lib-dev-configs/compare/v1.13.1...v2.0.0) (2019-07-23)


### Bug Fixes

* **async-shell:** added promises ([e868784](https://gitlab.com/revved/lib-dev-configs/commit/e868784))
* shelljs helper" ([51981f7](https://gitlab.com/revved/lib-dev-configs/commit/51981f7))
* **jesttimeout:** increased to 200000ms ([a897e52](https://gitlab.com/revved/lib-dev-configs/commit/a897e52))
* **shell.exec:** allowed options to be passed ([c4e8c98](https://gitlab.com/revved/lib-dev-configs/commit/c4e8c98))
* **shell.exec:** increased test coverage for errors ([e235bba](https://gitlab.com/revved/lib-dev-configs/commit/e235bba))


### Features

* **presets:** added dependency presets ([3493766](https://gitlab.com/revved/lib-dev-configs/commit/3493766))


### Tests

* **comments:** improved inline comments to explain the process ([03b722c](https://gitlab.com/revved/lib-dev-configs/commit/03b722c))


### BREAKING CHANGES

* **presets:** new commands for adding presets add-config-files , add-scripts , add-deps



### [1.13.1](https://gitlab.com/revved/lib-dev-configs/compare/v1.13.0...v1.13.1) (2019-07-16)


### Bug Fixes

* **test-async:** added bluebird promise.map to loop through test ([69c05ad](https://gitlab.com/revved/lib-dev-configs/commit/69c05ad)), closes [#4](https://gitlab.com/revved/lib-dev-configs/issues/4)
* **tests:** fixed the forEach async ([2e3a4c8](https://gitlab.com/revved/lib-dev-configs/commit/2e3a4c8))



## [1.13.0](https://gitlab.com/revved/lib-dev-configs/compare/v1.12.2...v1.13.0) (2019-07-16)


### Features

* **project-type-presets:** added preset directory ([1a0db95](https://gitlab.com/revved/lib-dev-configs/commit/1a0db95))



### [1.12.2](https://gitlab.com/revved/lib-dev-configs/compare/v1.12.1...v1.12.2) (2019-07-05)


### Bug Fixes

* added commitlint to config files ([ef135b8](https://gitlab.com/revved/lib-dev-configs/commit/ef135b8))



### [1.12.1](https://gitlab.com/revved/lib-dev-configs/compare/v1.12.0...v1.12.1) (2019-07-05)


### Bug Fixes

* add new line after water mark ([d708862](https://gitlab.com/revved/lib-dev-configs/commit/d708862))



## [1.12.0](https://gitlab.com/revved/lib-dev-configs/compare/v1.11.0...v1.12.0) (2019-07-05)


### Bug Fixes

* use # for comment chars for girlabci files ([2982d6a](https://gitlab.com/revved/lib-dev-configs/commit/2982d6a))


### Features

* [WIP] started working on not overriding non managed files ([8a18a9d](https://gitlab.com/revved/lib-dev-configs/commit/8a18a9d))
* respect non managed files ([e1b2a6d](https://gitlab.com/revved/lib-dev-configs/commit/e1b2a6d)), closes [#3](https://gitlab.com/revved/lib-dev-configs/issues/3)


### Tests

* added mocked mockFs to allow for console logs ([3d20059](https://gitlab.com/revved/lib-dev-configs/commit/3d20059))



## [1.11.0](https://gitlab.com/revved/lib-dev-configs/compare/v1.10.1...v1.11.0) (2019-07-03)


### Features

* added git add . to npm commit script ([934c52a](https://gitlab.com/revved/lib-dev-configs/commit/934c52a))



### [1.10.1](https://gitlab.com/revved/lib-dev-configs/compare/v1.10.0...v1.10.1) (2019-07-03)



## [1.10.0](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.11...v1.10.0) (2019-07-03)


### Features

* added gitlab config file ([cb325fd](https://gitlab.com/revved/lib-dev-configs/commit/cb325fd))



### [1.9.11](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.10...v1.9.11) (2019-07-02)


### Bug Fixes

* trying to fix build ([0ce6401](https://gitlab.com/revved/lib-dev-configs/commit/0ce6401))


### Tests

* :white_check_mark: 100% coverage ([4933e5a](https://gitlab.com/revved/lib-dev-configs/commit/4933e5a))
* added first test ([86f7ea5](https://gitlab.com/revved/lib-dev-configs/commit/86f7ea5))
* added jest config ([c0c4946](https://gitlab.com/revved/lib-dev-configs/commit/c0c4946))
* added mock-fs to write tests ([2b41b46](https://gitlab.com/revved/lib-dev-configs/commit/2b41b46))



### [1.9.10](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.9...v1.9.10) (2019-07-02)


### Bug Fixes

* :bug: testing changelog for fixes ([f55bcc0](https://gitlab.com/revved/lib-dev-configs/commit/f55bcc0))



### [1.9.9](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.8...v1.9.9) (2019-07-02)



### [1.9.8](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.6...v1.9.8) (2019-07-01)



### [1.9.6](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.5...v1.9.6) (2019-07-01)



### [1.9.5](https://gitlab.com/revved/lib-dev-configs/compare/v1.9.4...v1.9.5) (2019-07-01)



### [1.9.4](https://gitlab.com/revved/lib-dev-configs/compare/v1.3.3...v1.9.4) (2019-07-01)



### [1.3.2](https://gitlab.com/revved/lib-dev-configs/compare/v1.3.1...v1.3.2) (2019-07-01)



### [1.3.1](https://gitlab.com/revved/lib-dev-configs/compare/v1.3.0...v1.3.1) (2019-07-01)



## [1.3.0](https://gitlab.com/revved/lib-dev-configs/compare/v1.2.42...v1.3.0) (2019-06-28)


### Features

* :wrench: commit message linting ([036eead](https://gitlab.com/revved/lib-dev-configs/commit/036eead))
* commit message linting ([eee86e9](https://gitlab.com/revved/lib-dev-configs/commit/eee86e9))
* feat: automated changelog ([7cca29e](https://gitlab.com/revved/lib-dev-configs/commit/7cca29e))
